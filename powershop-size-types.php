<?php

/**
 * Plugin Name: Powershop Size Types
 * Plugin URI: https://peanutbutter.es
 * Description: Adds the size type when importing a product from Powershop to Woocommerce
 * Version: 1.0.2
 * Author: Pablo Giralt
 * Author URI: https://peanutbutter.es
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Domain Path: /lang
 * Text Domain: powershop_size_types
 */

 // inspired by https://wpruby.com/shipping-method-for-woocommerce-development-tutorial/

if ( ! defined( 'WPINC' ) ) {
	die;
}

/*
 * Check if WooCommerce and Powershop plugins are active
 */
if (
	in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) &&
	in_array( 'powershop/powershop.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	add_action( 'admin_init', function(){
		// Atributo de Woocommerce usado para los tipos de talla
		register_setting( 'powershop-settings-group', 'powershop_attribute_size_type' );
	} );

	add_action( 'powershop_settings_after', function(){

		$attributes = wc_get_attribute_taxonomies();
		?>

		<h2><?= __('Opciones de Tipos de talla', 'powershop');?></h2>

    <table class="form-table">

    	<tr valign="top">
		  	<th scope="row"><?= __('Usado para los tipos de talla', 'powershop');?></th>
		  	<td>
			  	<select name="powershop_attribute_size_type">
				  	<option><?= __('Selecciona un atributo', 'powershop'); ?></option>
				  	<?php if(!empty($attributes)){ ?>
					  	<?php foreach ($attributes as $attribute){ ?>
								<option value="<?= $attribute->attribute_id; ?>" <?= esc_attr(get_option('powershop_attribute_size_type')) == $attribute->attribute_id ? 'selected':''  ?>>
									<?= $attribute->attribute_name; ?>
								</option>
					  	<?php } ?>
				  	<?php } ?>
			  	</select>
		  	</td>
	    </tr>

    </table>
		<?php
	});


	// Add Size group attribute to product before import
	add_filter( 'powershop_product_import_data', function($data, $product){

		$size_type_attribute_id = esc_attr(get_option('powershop_attribute_size_type'));
		if(!$size_type_attribute_id){
			return $data;
		}

		$talla_unica_name = esc_attr(get_option('powershop_talla_unica'));
		$tallas_string = str_replace('||', '', str_replace($talla_unica_name, '', $product['Tallas'] ) );
		$tallas_string = rtrim ( $tallas_string, '|' );

		if($tallas_string!=='' && $tallas_string!==$talla_unica_name){

			switch ($tallas_string) {
		    case "00|01|02|03|04|05|06|07|08|09|10":
		    case "00|0|1|2|3|4|5|6|7|8|9|10":
		      $type = 1;
		      break;

		    case "15|17|19|21|23|25|27|29|31":
		      $type = 2;
		      break;

		    case "24|25|26|27|28|29|30|31|32|33|34":
		      $type = 3;
		      break;

		    case "30|31|32|33|34|36|38|40|42|44":
		      $type = 4;
		      break;

		    case "31|32|33|34|35|36|37|38|39|40|41|42":
		      $type = 5;
		      break;

		    case "34|36|38|40|42|44|46|48":
		      $type = 6;
		      break;

		    case "35|35M|36|36M|37|37M|38|38M|39|39M|40|40M|41|41M":
		      $type = 7;
		      break;

		    case "38|39|40|41|42|43|44|45|46":
		      $type = 8;
		      break;

		    case "40|40M|41|41M|42|42M|43|43M|44|44M|45|45M|46|46M":
		      $type = 9;
		      break;

		    case "40|42|44|46|48|50|52|54|56":
		      $type = 10;
		      break;

		    case "50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65":
		      $type = 11;
		      break;

		    case "6|8|10|12|14|16|18":
		      $type = 12;
		      break;

		    case "70|75|80|85|90|95|100|105":
		      $type = 13;
		      break;

		    case "70|75|80|85|90|95|100|105":
		      $type = 13;
		      break;

		    case "XXS|XS|S|M|L|XL|XXL|XS-S|S-M|M-L|L-XL":
		      $type = 14;
		      break;
		    default:
		    	$type = 15;
			}

			//@TODO - asignar el id del atributo dinamicamente
			$data['attributes'][] = [
				'id' => $size_type_attribute_id,
				'position' => 10,
				'visible' => false,
				'variation' => false,
				'options' => ['tipo '.$type]
			];

			//write_log($data['attributes']);
		}

		return $data;
	}, 10, 2 );


}


/*
 * Check for updates
 */ 
if ( is_admin() ) {
	require plugin_dir_path( __FILE__ ) . 'includes/plugin-update-checker/plugin-update-checker.php';
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'http://updates.peanutbutter.es/?action=get_metadata&slug=powershop-size-types',
		__FILE__, //Full path to the main plugin file or functions.php.
		'powershop-size-types'
	);
}
